package mandysax.tablayout

import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import mandysax.tablayout.databinding.BottomNavigactionItemBinding

class BottomNavigationItem constructor(
    @DrawableRes private val imageRes: Int,
    private val text: String
) : TabItemModel {
    @ColorInt
    private var mActiveColor: Int? = null

    @ColorInt
    private var mInActiveColor: Int? = null

    override fun setActiveColor(@ColorInt color: Int): BottomNavigationItem {
        mActiveColor = color
        return this
    }

    override fun setInActiveColor(@ColorInt color: Int): BottomNavigationItem {
        mInActiveColor = color
        return this
    }

    override fun getLayoutId(): Int = R.layout.bottom_navigaction_item

    override fun active(itemView: View, active: Boolean) {
        val binding = BottomNavigactionItemBinding.bind(itemView)
        binding.text.text = text
        when (active) {
            true ->
                mActiveColor
            false ->
                mInActiveColor
        }?.let {
            val icon = itemView.resources.getDrawable(imageRes, null)
            icon.setTint(it)
            binding.text.setCompoundDrawablesWithIntrinsicBounds(null, icon, null, null)
            binding.text.setTextColor(
                it
            )
        }
    }

}