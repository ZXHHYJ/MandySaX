package mandysax.media.model;

/**
 * @author Huang hao
 */
public interface ArtistModel {
    String getName();
}
