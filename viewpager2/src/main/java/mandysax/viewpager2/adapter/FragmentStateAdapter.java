package mandysax.viewpager2.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mandysax.fragment.Fragment;
import mandysax.fragment.FragmentView;
import mandysax.lifecycle.Lifecycle;
import mandysax.lifecycle.LifecycleObserver;
import mandysax.lifecycle.LifecycleOwner;
import mandysax.lifecycle.ViewTreeLifecycleOwner;

public class FragmentStateAdapter extends RecyclerView.Adapter<FragmentStateAdapter.FragmentViewHolder> {

    @NonNull
    @Override
    public FragmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Fragment fragment = createFragment(viewType);
        FragmentView itemView = new FragmentView(parent.getContext(), fragment);
        itemView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        itemView.setId(View.generateViewId());
        itemView.setSaveEnabled(false);
        return new FragmentViewHolder(itemView, fragment);
    }

    @Override
    public final int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull FragmentViewHolder holder, int position) {
    }

    @Override
    public void onViewAttachedToWindow(@NonNull FragmentViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (!holder.mFragment.isDetached())
            holder.mFragment.getFragmentManager().beginTransaction().show(holder.mFragment).commitNow();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull FragmentViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (!holder.mFragment.isDetached())
            holder.mFragment.getFragmentManager().beginTransaction().hide(holder.mFragment).commitNow();
    }

    public Fragment createFragment(int position) {
        return null;
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class FragmentViewHolder extends RecyclerView.ViewHolder {

        protected final Fragment mFragment;

        public FragmentViewHolder(View itemView, @NonNull Fragment fragment) {
            super(itemView);
            mFragment = fragment;
        }

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        LifecycleOwner owner = ViewTreeLifecycleOwner.get(recyclerView);
        if (owner != null)
            owner.getLifecycle().addObserver(new LifecycleObserver() {
                @Override
                public void observer(@NonNull Lifecycle.Event event) {
                    LifecycleObserver.super.observer(event);
                    if (event == Lifecycle.Event.ON_DESTROY)
                        recyclerView.setAdapter(null);
                }
            });
    }

}
