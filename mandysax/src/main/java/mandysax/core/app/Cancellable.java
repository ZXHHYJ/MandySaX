package mandysax.core.app;

interface Cancellable {

    void cancel();
}
