package mandysax.lifecycle;

import androidx.annotation.NonNull;

public interface FullLifecycleObserver extends LifecycleObserver {

    @Override
    default void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {
        LifecycleObserver.super.onStateChanged(source, event);
        switch (event) {
            case ON_CREATE:
                onCreate(source);
                break;
            case ON_START:
                onStart(source);
                break;
            case ON_RESUME:
                onResume(source);
                break;
            case ON_PAUSE:
                onPause(source);
                break;
            case ON_STOP:
                onStop(source);
                break;
            case ON_DESTROY:
                onDestroy(source);
        }
    }

    default void onCreate(@NonNull LifecycleOwner owner) {
    }

    default void onStart(@NonNull LifecycleOwner owner) {
    }

    default void onResume(@NonNull LifecycleOwner owner) {
    }

    default void onPause(@NonNull LifecycleOwner owner) {
    }

    default void onStop(@NonNull LifecycleOwner owner) {
    }

    default void onDestroy(@NonNull LifecycleOwner owner) {
    }

}
