package mandysax.lifecycle;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class LifecycleService extends Service implements LifecycleOwner {

    private final LifecycleRegistry mLifecycle = new LifecycleRegistry(this);

    @Nullable
    @Override
    @CallSuper
    public IBinder onBind(Intent intent) {
        mLifecycle.markState(Lifecycle.Event.ON_START);
        return null;
    }

    @SuppressWarnings("deprecation")
    @CallSuper
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        mLifecycle.markState(Lifecycle.Event.ON_START);
    }

    // this method is added only to annotate it with @CallSuper.
    // In usual service super.onStartCommand is no-op, but in LifecycleService
    // it results in mDispatcher.onServicePreSuperOnStart() call, because
    // super.onStartCommand calls onStart().
    @CallSuper
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @CallSuper
    @Override
    public void onCreate() {
        super.onCreate();
        mLifecycle.markState(Lifecycle.Event.ON_CREATE);
    }

    @CallSuper
    @Override
    public void onDestroy() {
        super.onDestroy();
        mLifecycle.markState(Lifecycle.Event.ON_STOP);
        mLifecycle.markState(Lifecycle.Event.ON_DESTROY);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycle;
    }
}
