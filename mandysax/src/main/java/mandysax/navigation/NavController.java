package mandysax.navigation;

import android.content.res.TypedArray;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;

import mandysax.core.R;
import mandysax.core.app.OnBackPressedCallback;
import mandysax.fragment.Fragment;
import mandysax.fragment.FragmentTransaction;
import mandysax.fragment.FragmentViewLifecycleOwner;
import mandysax.lifecycle.FullLifecycleObserver;
import mandysax.lifecycle.Lifecycle;
import mandysax.lifecycle.LifecycleOwner;
import mandysax.lifecycle.ViewModelProviders;
import mandysax.navigation.fragment.NavHostFragment;

/**
 * 导航控制器
 *
 * @author Huang hao
 */
public final class NavController {

    private final NavHostFragment mNavHost;

    private final NavControllerViewModel mViewModel;

    /**
     * @param navHostFragment 带导航的NavHostFragment
     */
    public NavController(@NonNull NavHostFragment navHostFragment) {
        mNavHost = navHostFragment;
        mViewModel = ViewModelProviders.of(navHostFragment.getViewLifecycleOwner()).get(NavControllerViewModel.class);
    }

    @MainThread
    public void navigate(@NonNull Fragment awaitingFragment) {
        new NavFragmentOnStartCallback(mNavHost.getViewLifecycleOwner(), awaitingFragment, 0);
    }

    @MainThread
    public void navigate(@NonNull Fragment awaitingFragment, @StyleRes int animResId) {
        new NavFragmentOnStartCallback(mNavHost.getViewLifecycleOwner(), awaitingFragment, animResId);
    }

    @NonNull
    private FragmentTransaction beginTransaction() {
        return mNavHost.getChildFragmentManager().beginTransaction();
    }

    private void navigate(Fragment awaitingFragment, int fragmentEnterAnim, int fragmentExitAnim, int fragmentPopEnterAnim, int fragmentPopExitAnim) {
        FragmentTransaction fragmentTransaction = beginTransaction();
        fragmentTransaction.setCustomAnimations(fragmentEnterAnim, fragmentExitAnim, 0, 0);
        int navId = mNavHost.getRoot().getId();
        Fragment nowFragment = mViewModel.getNowFragment();
        if (nowFragment != null) {
            fragmentTransaction.hide(nowFragment);
        }
        fragmentTransaction.add(navId, awaitingFragment);
        fragmentTransaction.commit();
        mViewModel.add(awaitingFragment);
        awaitingFragment.getViewLifecycleOwner().getLifecycle().addObserver(new FullLifecycleObserver() {

            private boolean mAddBackCallback = false;

            @Override
            public void observer(@NonNull Lifecycle.Event event) {
                FullLifecycleObserver.super.observer(event);
                switch (event) {
                    case ON_DESTROY:
                    case ON_CREATE:
                    case ON_PAUSE:
                    case ON_STOP:
                        break;
                    default:
                        if (!mAddBackCallback) {
                            mAddBackCallback = true;
                            awaitingFragment.requireActivity().getOnBackPressedDispatcher().addCallback(mNavHost.getViewLifecycleOwner(), new OnBackPressedCallback(true) {
                                @Override
                                public void handleOnBackPressed() {
                                    //返回栈中已没有任务，此时的返回事件应该传递给activity
                                    if (mViewModel.getLastFragment() == null) {
                                        remove();
                                        //消费此事件
                                        navigateUp();
                                        return;
                                    }
                                    //正常情况，移除当前显示的fragment，并恢复之前的fragment
                                    //消费此事件
                                    remove();
                                    FragmentTransaction fragmentTransaction = beginTransaction();
                                    fragmentTransaction.setCustomAnimations(fragmentPopEnterAnim, 0, 0, fragmentPopExitAnim);
                                    fragmentTransaction.remove(awaitingFragment);
                                    fragmentTransaction.show(mViewModel.getLastFragment());
                                    fragmentTransaction.commit();
                                    mViewModel.removeLast();
                                }
                            });
                        }
                }
            }

        });
    }

    /**
     * run Activity.onBackPressed()
     */
    public void navigateUp() {
        mNavHost.requireActivity().onBackPressed();
    }

    private class NavFragmentOnStartCallback implements FullLifecycleObserver {

        private final Fragment mNavFragment;

        @StyleRes
        private final int mAnimResId;

        NavFragmentOnStartCallback(@NonNull FragmentViewLifecycleOwner viewLifecycleOwner, Fragment navFragment, @StyleRes int animResId) {
            mNavFragment = navFragment;
            mAnimResId = animResId;
            viewLifecycleOwner.getLifecycle().addObserver(this);
        }

        @Override
        public void onStart(@NonNull LifecycleOwner owner) {
            FullLifecycleObserver.super.onStart(owner);
            if (mAnimResId != 0) {
                int[] attr = new int[]{R.attr.fragmentEnterAnim, R.attr.fragmentExitAnim, R.attr.fragmentPopEnterAnim, R.attr.fragmentPopExitAnim};
                TypedArray array = mNavHost.requireActivity().getTheme().obtainStyledAttributes(mAnimResId, attr);
                navigate(mNavFragment, array.getResourceId(0, 0), array.getResourceId(1, 0), array.getResourceId(2, 0), array.getResourceId(3, 0));
                array.recycle();
            } else
                navigate(mNavFragment, 0, 0, 0, 0);
            owner.getLifecycle().removeObserver(this);
        }

    }

}
