package mandysax.navigation;

import android.view.View;

import androidx.annotation.NonNull;

import mandysax.navigation.fragment.NavHostFragmentView;

/**
 * 用于查找导航
 *
 * @author Huang hao
 */
public final class Navigation {

    /**
     * @param view 通过哪个view开始查找
     * @return 找到的距离最近的导航控制器
     */
    @NonNull
    public static NavController findViewNavController(View view) {
        View parent = view;
        NavHostFragmentView host = null;
        while (host == null && parent != null) {
            if (parent instanceof NavHostFragmentView) {
                host = ((NavHostFragmentView) parent);
            }
            parent = (View) parent.getParent();
        }
        if (host == null)
            throw new NullPointerException("No NavHostFragment found available above view");
        return host.getNavHostFragment().getNavController();
    }

}
