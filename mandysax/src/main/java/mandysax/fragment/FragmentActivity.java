package mandysax.fragment;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.Contract;

import mandysax.core.app.ComponentActivity;
import mandysax.core.view.LayoutInflaterCompat;
import mandysax.lifecycle.ViewModelProviders;

/**
 * @author Huang hao
 */
public class FragmentActivity extends ComponentActivity {

    //记一个不可以忘记的朋友<晓柳>

    private FragmentManagerViewModel mHost;

    private boolean mResumed = false;

    @NonNull
    @Contract(" -> new")
    public final FragmentManager getFragmentPlusManager() {
        return getFragmentController().getFragmentManager(null);
    }

    private FragmentStateManager getFragmentStateManager() {
        return getFragmentController();
    }

    private FragmentManagerViewModel getFragmentHost() {
        return mHost == null ? mHost = ViewModelProviders.of(this).get(FragmentManagerViewModel.class) : mHost;
    }

    FragmentController getFragmentController() {
        return getFragmentHost().mController;
    }

    @Override
    public void onMultiWindowModeChanged(boolean isInMultiWindowMode, Configuration newConfig) {
        super.onMultiWindowModeChanged(isInMultiWindowMode, newConfig);
        getFragmentStateManager().dispatchMultiWindowModeChanged(isInMultiWindowMode, newConfig);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getFragmentStateManager().dispatchConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory2(super.getLayoutInflater(), new FragmentLayoutInflaterFactory());
        super.onCreate(savedInstanceState);
        getFragmentStateManager().dispatchAttach(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFragmentStateManager().dispatchResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getFragmentStateManager().dispatchStart();
        if (!mResumed) {
            getFragmentStateManager().dispatchResumeFragment();
            mResumed = true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        getFragmentStateManager().dispatchPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getFragmentStateManager().dispatchStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getFragmentStateManager().dispatchDestroy();
        if (!isChangingConfigurations()) {
            getViewModelStore().clear();
        }
    }
}