package mandysax.fragment;

import android.util.ArrayMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

class FragmentStore {

    private final HashMap<String, Store> mStore = new HashMap<>();
    private final CopyOnWriteArrayList<Fragment> mFragments = new CopyOnWriteArrayList<>();

    private Store findStore(Store store, String tag) {
        if (store == null) {
            return null;
        }
        if (store.fragment.getTag().equals(tag)) {
            return store;
        }
        if (store.stores == null) {
            return null;
        }
        for (Store child : store.stores) {
            Store target = findStore(child, tag);
            if (target != null) {
                return target;
            }
        }
        return null;
    }

    @Nullable
    final Store findStore(String tag) {
        for (Store value : mStore.values()) {
            Store store = findStore(value, tag);
            if (store != null) {
                return store;
            }
        }
        return null;
    }

    final void addFragment(@NonNull Fragment fragment) {
        Store store = new Store();
        store.fragment = fragment;
        mStore.put(fragment.getTag(), store);
        mFragments.add(fragment);
        add(fragment);
    }

    final void addFragment(@NonNull Fragment parentFragment, Fragment fragment) {
        Store store = new Store();
        store.fragment = fragment;
        store.parentStore = findStore(parentFragment.getTag());
        if (store.parentStore != null) {
            if (store.parentStore.stores == null) {
                store.parentStore.stores = new CopyOnWriteArrayList<>();
            }
            store.parentStore.stores.add(store);
        }
        mStore.put(fragment.getTag(), store);
        mFragments.add(fragment);
        add(fragment);
    }

    @Nullable
    final Fragment findFragmentByTag(String tag) {
        Store store = findStore(tag);
        return store == null ? null : store.fragment;
    }

    final void removeFragment(@NonNull Fragment fragment) {
        Store store = findStore(fragment.getTag());
        if (store != null) {
            if (store.stores != null) {
                for (Store store1 : store.stores) {
                    removeFragment(store1.fragment);
                }
            }
            if (store.parentStore != null && store.parentStore.stores != null) {
                store.parentStore.stores.remove(store);
                store.parentStore.stores = null;
            }
            store.fragment = null;
            mFragments.remove(fragment);
            mStore.remove(fragment.getTag());
            remove(fragment);
        }
    }

    void add(@NonNull Fragment fragment) {

    }

    void remove(@NonNull Fragment fragment) {

    }

    final CopyOnWriteArrayList<Fragment> values() {
        return mFragments;
    }

    void clear() {
        mFragments.clear();
        mStore.clear();
    }

    static class Store {
        Store parentStore = null;
        Fragment fragment = null;
        CopyOnWriteArrayList<Store> stores = null;
    }

}
