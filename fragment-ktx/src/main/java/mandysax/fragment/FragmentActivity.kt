package mandysax.fragment

import mandysax.lifecycle.FullLifecycleObserver
import mandysax.lifecycle.LifecycleOwner
import mandysax.lifecycle.ViewModel
import mandysax.lifecycle.ViewModelProviders
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

inline fun <reified VB : ViewModel> FragmentActivity.viewModels() = lazy {
    return@lazy ViewModelProviders.of(
        this
    )[VB::class.java]
}

class FragmentActivityViewModelDelegate<VM : ViewModel>(private val clazz: Class<VM>) :
    ReadOnlyProperty<Fragment, VM> {

    private var mViewModel: VM? = null

    override fun getValue(thisRef: Fragment, property: KProperty<*>): VM {
        if (mViewModel == null) {
            mViewModel =
                ViewModelProviders.of(
                    thisRef.requireActivity()
                )[clazz]
            object : FullLifecycleObserver {
                override fun onDestroy(owner: LifecycleOwner) {
                    super.onDestroy(owner)
                    mViewModel = null
                }
            }.let {
                thisRef.lifecycle.addObserver(it)
                thisRef.viewLifecycleOwner.lifecycle.addObserver(it)
            }
        }
        return mViewModel!!
    }

}

inline fun <reified VB : ViewModel> activityViewModels() =
    FragmentActivityViewModelDelegate(VB::class.java)