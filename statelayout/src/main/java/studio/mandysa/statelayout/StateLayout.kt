package studio.mandysa.statelayout

import android.content.Context
import android.content.res.TypedArray
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout

class StateLayout(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    companion object {
        @JvmStatic
        var emptyLayout: Int = -1

        @JvmStatic
        var loadingLayout: Int = -1

        @JvmStatic
        var errorLayout: Int = -1

        @JvmStatic
        var empty: (View.() -> Unit)? = null

        @JvmStatic
        var loading: (View.() -> Unit)? = null

        @JvmStatic
        var error: (View.() -> Unit)? = null

        private var retryId = View.NO_ID

        @JvmName("setRetryId")
        fun setRetryId(retryId: Int) {
            this.retryId = retryId
        }
    }

    private var mEmpty: (View.() -> Unit)? = empty

    private var mLoading: (View.() -> Unit)? = loading

    private var mError: (View.() -> Unit)? = error

    private var mContent: (View.() -> Unit)? = null

    private var mEmptyLayoutId: Int

    private var mLoadingLayoutId: Int

    private var mErrorLayoutId: Int

    private var mEmptyView: View? = null

    private var mLoadingView: View? = null

    private var mErrorView: View? = null

    private var mContentView: View? = null

    private var mRetryId = retryId

    fun setEmptyLayoutId(id: Int) {
        mEmptyLayoutId = id
    }

    fun setLoadingLayoutId(id: Int) {
        mLoadingLayoutId = id
    }

    fun setErrorLayoutId(id: Int) {
        mErrorLayoutId = id
    }

    fun setRetryId(retryId: Int) {
        mRetryId = retryId
    }

    override fun addView(child: View, index: Int, params: ViewGroup.LayoutParams?) {
        super.addView(child, index, params)
        if (mContentView == null) mContentView = child.also { child.visibility = View.GONE }
    }

    init {
        val typedArray: TypedArray =
            context.obtainStyledAttributes(attrs, R.styleable.StateLayout)
        mEmptyLayoutId =
            typedArray.getResourceId(R.styleable.StateLayout_emptyLayoutId, emptyLayout)
        mLoadingLayoutId =
            typedArray.getResourceId(R.styleable.StateLayout_loadingLayoutId, loadingLayout)
        mErrorLayoutId =
            typedArray.getResourceId(R.styleable.StateLayout_errorLayoutId, errorLayout)
        typedArray.recycle()
    }

    /**
     * 设置显示内容视图执行事件
     */
    fun showContent(block: View.() -> Unit) {
        mContent = block
    }

    /**
     * 设置显示空视图执行事件
     */
    fun showEmpty(block: View.() -> Unit) {
        mEmpty = block
    }

    /**
     * 设置显示加载试图执行事件
     */
    fun showLoading(block: View.() -> Unit) {
        mLoading = block
    }

    /**
     * 设置显示错误视图执行事件
     */
    fun showError(block: View.() -> Unit) {
        mError = block
    }

    fun showEmptyState() {
        mainThread {
            if (mEmptyView == null)
                addView(
                    LayoutInflater.from(context).inflate(mEmptyLayoutId, this, false)
                        .also {
                            mEmptyView = it
                            setRetryEvent(it)
                        }
                )
            hideOtherViews(mEmptyView)?.apply {
                visibility = View.VISIBLE
                empty?.invoke(this)
                mEmpty?.invoke(this)
            }
        }
    }

    fun showLoadingState() {
        mainThread {
            if (mLoadingView == null)
                addView(
                    LayoutInflater.from(context).inflate(mLoadingLayoutId, this, false)
                        .also {
                            mLoadingView = it
                        }
                )
            hideOtherViews(mLoadingView)?.apply {
                visibility = View.VISIBLE
                loading?.invoke(this)
                mLoading?.invoke(this)
            }
        }
    }

    fun showErrorState() {
        mainThread {
            if (mErrorView == null)
                addView(
                    LayoutInflater.from(context).inflate(mErrorLayoutId, this, false)
                        .also {
                            mErrorView = it
                            setRetryEvent(it)
                        }
                )
            hideOtherViews(mErrorView)?.apply {
                visibility = View.VISIBLE
                error?.invoke(this)
                mError?.invoke(this)
            }
        }
    }

    fun showContentState() {
        mainThread {
            hideOtherViews(mContentView)?.apply {
                visibility = View.VISIBLE
                mContent?.invoke(this)
            }
        }
    }

    private fun setRetryEvent(view: View) {
        if (mRetryId != View.NO_ID)
            view.findViewById<View?>(mRetryId)?.setOnClickListener {
                showLoadingState()
            }
    }

    private fun hideOtherViews(view: View?): View? {
        for (index in 0 until childCount) {
            getChildAt(index).let {
                if (it.equals(view)) {
                    return@let
                }
                it.visibility = GONE
            }
        }
        return view
    }

    private inline fun mainThread(crossinline block: () -> Unit) {
        if (isMainThread()) {
            block.invoke()
            return
        }
        this.post { block.invoke() }
    }

    private fun isMainThread(): Boolean {
        return Looper.getMainLooper() == Looper.myLooper()
    }

}