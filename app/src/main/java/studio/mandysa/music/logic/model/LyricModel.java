package studio.mandysa.music.logic.model;

import mandysax.anna2.annotation.Value;

/**
 * @author Huang hao
 */
public class LyricModel {
    @Value("lyric")
    public String lyric;
}
