package studio.mandysa.music.logic.model;

import mandysax.anna2.annotation.Value;

/**
 * @author Huang hao
 */
public class SearchSingerModel {
    @Value("id")
    public String id;

    @Value("name")
    public String name;

    @Value("img1v1Url")//头像
    public String picUrl;
}
