package studio.mandysa.music.logic.model

import mandysax.anna2.annotation.Value

class SingerAlbumModel {

    @Value("id")
    val id = ""

    @Value("name")
    val name = ""

    @Value("artist")
    val album: AlbumModel? = null
}