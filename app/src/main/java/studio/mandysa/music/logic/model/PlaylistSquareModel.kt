package studio.mandysa.music.logic.model

import mandysax.anna2.annotation.Value

/**
 * @author Huang hao
 */
class PlaylistSquareModel {

    @Value("result")
    val playlist: List<PlaylistModel>? = null

}