package studio.mandysa.music.logic.model

import mandysax.anna2.annotation.Value

class SingerHotSongModel {
    @Value("songs")
    val list: List<MusicModel>? = null
}