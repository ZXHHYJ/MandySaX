package studio.mandysa.music.logic.model.design;

public final class SubTitleModel {
    private final String mTitle;

    public SubTitleModel(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }
}
