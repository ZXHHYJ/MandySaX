package studio.mandysa.music.application

import android.app.Application
import android.content.Intent
import simon.tuke.Tuke
import studio.mandysa.music.R
import studio.mandysa.music.logic.ktx.playManager
import studio.mandysa.music.service.MediaPlayService
import studio.mandysa.music.service.playmanager.PlayManager
import studio.mandysa.statelayout.StateLayout

class AppApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        Tuke.init(this)

        StateLayout.let {
            it.loadingLayout = R.layout.layout_loading
            it.emptyLayout = R.layout.layout_empty
            it.errorLayout = R.layout.layout_error
            it.setRetryId(R.id.cl_error_check)
        }

        PlayManager.init(this) //初始化播放管理器
        playManager {
            pauseLiveData()
                .observeForever {
                    if (it == false)
                        startPlayerService()
                }
            changeMusicLiveData()
                .observeForever {
                    if (it != null)
                        startPlayerService()
                }
        }
    }

    private fun startPlayerService() {
        if (MediaPlayService.instance == null) {
            startForegroundService(
                Intent(
                    this@AppApplication,
                    MediaPlayService::class.java
                )
            )
        }
    }

}