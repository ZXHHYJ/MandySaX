package studio.mandysa.music.service.playmanager.model;

/**
 * @author Huang hao
 */
public interface ArtistModel {
    String getId();

    String getName();
}
