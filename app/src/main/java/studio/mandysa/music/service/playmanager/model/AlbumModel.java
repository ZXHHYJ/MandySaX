package studio.mandysa.music.service.playmanager.model;

public interface AlbumModel {

    String getId();

    String getCoverUrl();

    String getName();
}
