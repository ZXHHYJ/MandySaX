package studio.mandysa.music.ui.activity.main.fragment.play

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.SeekBar
import mandysax.fragment.Fragment
import mandysax.lifecycle.FullLifecycleObserver
import mandysax.lifecycle.LifecycleOwner
import mandysax.lifecycle.livedata.Observer
import studio.mandysa.music.R
import studio.mandysa.music.databinding.FragmentPlayingBinding
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.ktx.playManager
import studio.mandysa.music.service.playmanager.ktx.allArtist
import studio.mandysa.music.ui.activity.main.fragment.play.helper.VolumeChangeHelper


class PlayingFragment : Fragment() {
    private val mBinding: FragmentPlayingBinding by viewBinding()

    private fun getViewAnim(pause: Boolean): Animation {
        val smallScale = if (pause) 0.9F else 1F
        val oneScale = if (!pause) 0.9F else 1F
        val scaleAnim = ScaleAnimation(
            oneScale,
            smallScale,
            oneScale,
            smallScale,
            mBinding.musicCoverCardView.width / 2f,
            mBinding.musicCoverCardView.width / 2f
        )
        scaleAnim.duration = 200
        scaleAnim.fillAfter = true
        return scaleAnim
    }

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        mBinding.let { it ->
            //音乐进度监听
            val musicProObs = Observer<Int> { progress ->
                it.playbackSeekbar.progress = progress
            }
            playManager {
                viewLifecycleOwner.lifecycle.addObserver(object : FullLifecycleObserver {
                    override fun onStart(owner: LifecycleOwner) {
                        super.onStart(owner)
                        it.musicCoverCardView.startAnimation(getViewAnim(pauseLiveData().value).also {
                            it.duration = 0
                        })
                    }
                })
                //播放暂停按钮icon更新
                pauseLiveData().observe(viewLifecycleOwner) { pause ->
                    it.playOrPause.setImageResource(if (pause) R.drawable.ic_play else R.drawable.ic_pause)
                    it.musicCoverCardView.startAnimation(getViewAnim(pause))
                }
                //更新当前播放歌曲的信息
                changeMusicLiveData().observe(viewLifecycleOwner) { model ->
                    it.songName.text = model.title
                    it.singerName.text = model.artist.allArtist()
                    it.musicCover.load(model.coverUrl)
                }
                //更新播放进度
                playingMusicDurationLiveData()
                    .observe(viewLifecycleOwner) { duration ->
                        it.playbackSeekbar.max = duration
                    }
                playingMusicProgressLiveData()
                    .observe(viewLifecycleOwner, musicProObs)
                //播放进度条
                it.playbackSeekbar.setOnSeekBarChangeListener(object :
                    SeekBar.OnSeekBarChangeListener {
                    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                    }

                    override fun onStartTrackingTouch(p0: SeekBar?) {
                        playingMusicProgressLiveData()
                            .removeObserver(musicProObs)
                    }

                    override fun onStopTrackingTouch(p0: SeekBar) {
                        seekTo(p0.progress)
                        play()
                        playingMusicProgressLiveData()
                            .observe(viewLifecycleOwner, musicProObs)
                    }

                })
                //上一曲
                it.playSkipPrevious.setOnClickListener {
                    skipToPrevious()
                }
                //播放暂停
                it.playOrPause.setOnClickListener {
                    if (pauseLiveData().value == true)
                        play()
                    else pause()
                }
                //下一曲
                it.playSkipNext.setOnClickListener {
                    skipToNext()
                }
            }
            //音量条
            mBinding.volumeSeekbar.let {
                val volumeChangeHelper = VolumeChangeHelper(context)
                volumeChangeHelper.registerVolumeChangeListener(object :
                    VolumeChangeHelper.VolumeChangeListener {
                    override fun onVolumeDownToMin() {
                        it.progress = 0
                    }

                    override fun onVolumeUp() {
                        it.progress = volumeChangeHelper.getStreamVolume()
                    }

                })
                it.max = volumeChangeHelper.getStreamMaxVolume()
                it.progress = volumeChangeHelper.getStreamVolume()
                it.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                        if (p1 != volumeChangeHelper.getStreamVolume())
                            volumeChangeHelper.setStreamMusic(p1)
                    }

                    override fun onStartTrackingTouch(p0: SeekBar?) {

                    }

                    override fun onStopTrackingTouch(p0: SeekBar) {

                    }

                })
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }

}