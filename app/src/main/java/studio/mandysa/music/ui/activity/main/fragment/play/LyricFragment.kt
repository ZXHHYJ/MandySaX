package studio.mandysa.music.ui.activity.main.fragment.play

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mandysax.anna2.exception.AnnaException
import mandysax.fragment.Fragment
import mandysax.lifecycle.coroutineScope
import studio.mandysa.jiuwo.utils.recyclerAdapter
import studio.mandysa.music.databinding.FragmentLyricBinding
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.ktx.playManager
import studio.mandysa.music.logic.model.NeteaseCloudMusicApi
import studio.mandysa.music.logic.network.ServiceCreator
import studio.mandysa.music.service.playmanager.ktx.allArtist
import studio.mandysa.music.ui.activity.main.fragment.all.songmenu.SongMenuFragment

class LyricFragment : Fragment() {

    private val mBinding: FragmentLyricBinding by viewBinding()

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        playManager {
            changeMusicLiveData()
                .observe(viewLifecycleOwner) { model ->
                    mBinding.toolbar.apply {
                        songName.text = model.title
                        songSingerName.text = model.artist.allArtist()
                        songCover.load(
                            model.coverUrl
                        )
                        more.setOnClickListener {
                            SongMenuFragment(changeMusicLiveData().value).show(childFragmentManager)
                        }
                    }
                    mBinding.lyricView.recyclerAdapter.clearModels()
                    viewLifecycleOwner.lifecycle.coroutineScope.launch(Dispatchers.IO) {
                        try {
                            val t =
                                ServiceCreator.create(NeteaseCloudMusicApi::class.java)
                                    .getLyric(model.id)
                                    .execute()
                            withContext(Dispatchers.Main) {
                                mBinding.lyricView.lyric = t.lyric
                            }
                        } catch (e: AnnaException) {
                            withContext(Dispatchers.Main) {
                                mBinding.lyricView.lyric = "[00:00:00]获取歌词错误"
                            }
                        }
                    }
                }
            playingMusicProgressLiveData()
                .observe(viewLifecycleOwner) {
                    mBinding.lyricView.time = it
                }
            mBinding.lyricView.getChildTimeLiveData().observe(viewLifecycleOwner) { pos ->
                let {
                    it.seekTo(pos)
                    it.play()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }
}