package studio.mandysa.music.ui.activity.main.fragment.search.realsearch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mandysax.anna2.exception.AnnaException
import mandysax.fragment.Fragment
import mandysax.lifecycle.coroutineScope
import mandysax.lifecycle.livedata.MutableLiveData
import mandysax.navigation.Navigation
import studio.mandysa.jiuwo.utils.addFooter
import studio.mandysa.jiuwo.utils.linear
import studio.mandysa.jiuwo.utils.recyclerAdapter
import studio.mandysa.jiuwo.utils.setup
import studio.mandysa.music.R
import studio.mandysa.music.databinding.ItemSingerBinding
import studio.mandysa.music.databinding.LayoutStateRvBinding
import studio.mandysa.music.logic.ktx.addBottom
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.ktx.markByColor
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.model.NeteaseCloudMusicApi
import studio.mandysa.music.logic.model.SearchSingerModel
import studio.mandysa.music.logic.network.ServiceCreator
import studio.mandysa.music.ui.activity.main.fragment.all.singerpage.SingerPageFragment

class RealSearchSingerFragment(private val mSearchContentLiveData: MutableLiveData<String>) :
    Fragment() {

    private val mBinding: LayoutStateRvBinding by viewBinding()

    private var mPage = 1

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        ViewCompat.setOnApplyWindowInsetsListener(mBinding.root) { _, insets ->
            val navigationBarSize =
                insets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
            mBinding.recycler.setPadding(
                0,
                0,
                0,
                context.resources.getDimensionPixelSize(R.dimen.controller_height) + context.resources.getDimensionPixelSize(
                    R.dimen.nav_height
                ) + navigationBarSize
            )
            insets
        }
        mBinding.stateLayout.showLoading {
            nextPage()
        }
        mBinding.recycler.addBottom {
            nextPage()
        }
        mBinding.recycler.linear().setup {
            addType<SearchSingerModel>(R.layout.item_singer) {
                ItemSingerBinding.bind(itemView).run {
                    val model = getModel<SearchSingerModel>()
                    singerAvatar.load(model.picUrl)
                    singerName.text = model.name
                    singerName.markByColor(mSearchContentLiveData.value)
                    itemView.setOnClickListener {
                        Navigation.findViewNavController(it)
                            .navigate(SingerPageFragment(model.id))
                    }
                }
            }
        }
        mSearchContentLiveData.observe(viewLifecycleOwner) { it ->
            if (it.isNotEmpty()) {
                mBinding.let {
                    if (it.recycler.adapter != null && it.recycler.recyclerAdapter.mModel != null) {
                        it.recycler.recyclerAdapter.clearModels()
                        mPage = 1
                    }
                    it.stateLayout.showLoadingState()
                }
            }
        }
    }

    private fun nextPage() {
        viewLifecycleOwner.lifecycle.coroutineScope.launch(Dispatchers.IO) {
            try {
                ServiceCreator.create(NeteaseCloudMusicApi::class.java).let { it ->
                    val model =
                        it.searchSinger(
                            mSearchContentLiveData.value,
                            (mPage - 1) * 30,
                            100
                        ).execute()
                    withContext(Dispatchers.Main) {
                        model.forEach {
                            mBinding.recycler.addFooter(it)
                        }
                        mBinding.stateLayout.showContentState()
                        mPage++
                    }
                }
            } catch (e: AnnaException) {
                mBinding.stateLayout.showErrorState()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }
}