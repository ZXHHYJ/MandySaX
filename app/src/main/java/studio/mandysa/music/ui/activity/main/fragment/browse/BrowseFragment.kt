package studio.mandysa.music.ui.activity.main.fragment.browse

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mandysax.anna2.exception.AnnaException
import mandysax.fragment.Fragment
import mandysax.fragment.activityViewModels
import mandysax.lifecycle.coroutineScope
import mandysax.navigation.Navigation
import studio.mandysa.jiuwo.utils.addModels
import studio.mandysa.jiuwo.utils.linear
import studio.mandysa.jiuwo.utils.recyclerAdapter
import studio.mandysa.jiuwo.utils.setup
import studio.mandysa.music.R
import studio.mandysa.music.databinding.*
import studio.mandysa.music.logic.decoration.HorizontalAlbumItemDecoration
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.model.*
import studio.mandysa.music.logic.model.design.SubTitleModel
import studio.mandysa.music.logic.model.design.TitleModel
import studio.mandysa.music.logic.network.ServiceCreator
import studio.mandysa.music.service.playmanager.PlayManager
import studio.mandysa.music.service.playmanager.ktx.allArtist
import studio.mandysa.music.ui.event.EventViewModel
import studio.mandysa.music.ui.activity.main.fragment.all.playlist.PlaylistFragment
import studio.mandysa.music.ui.activity.main.fragment.all.songmenu.SongMenuFragment


class BrowseFragment : Fragment() {
    private val mBinding: LayoutStateRvBinding by viewBinding()

    private val mEvent: EventViewModel by activityViewModels()

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        ViewCompat.setOnApplyWindowInsetsListener(mBinding.root) { _, insets ->
            val navigationBarSize =
                insets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
            mBinding.recycler.setPadding(
                0,
                0,
                0,
                context.resources.getDimensionPixelSize(R.dimen.controller_height) + context.resources.getDimensionPixelSize(
                    R.dimen.nav_height
                ) + navigationBarSize
            )
            insets
        }
        mBinding.recycler.linear().setup {
            addType<SubTitleModel>(R.layout.item_sub_title) {
                ItemSubTitleBinding.bind(itemView).title.text =
                    getModel<SubTitleModel>().title
            }
            addType<TitleModel>(R.layout.item_title) {
                ItemTitleBinding.bind(itemView).titleTv.text =
                    getModel<TitleModel>().title
            }
            addType<RecommendSongs.RecommendSong>(R.layout.item_song) {
                val model = getModel<RecommendSongs.RecommendSong>()
                ItemSongBinding.bind(itemView).apply {
                    songIndex.text = (modelPosition + 1).toString()
                    songName.text = model.title
                    songSingerName.text = model.artist.allArtist()
                    itemView.setOnClickListener {
                        PlayManager.loadPlaylist(
                            models,
                            modelPosition
                        )
                    }
                    more.setOnClickListener {
                        SongMenuFragment(model).show(childFragmentManager)
                    }
                }
            }
            val pagerSnapHelper = PagerSnapHelper()
            addType<BannerModels>(R.layout.layout_rv) {
                LayoutRvBinding.bind(itemView).recycler.apply {
                    pagerSnapHelper.attachToRecyclerView(this)
                    linear(orientation = RecyclerView.HORIZONTAL).setup {
                        addType<BannerModels.BannerModel>(R.layout.item_banner) {
                            val model = getModel<BannerModels.BannerModel>()
                            ItemBannerBinding.bind(itemView).let { it ->
                                it.title.text = model.typeTitle
                                it.planIv.load(
                                    model.pic
                                )
                                it.planIv.setOnClickListener {
                                    when (model.targetType) {
                                        3000 -> {
                                            val uri: Uri = Uri.parse(model.url)
                                            val intent = Intent(Intent.ACTION_VIEW, uri)
                                            startActivity(intent)
                                        }
                                        1000 -> {
                                            Navigation.findViewNavController(it)
                                                .navigate(
                                                    PlaylistFragment(model.encodeId)
                                                )
                                        }
                                        1 -> {

                                        }
                                        else -> {

                                        }
                                    }
                                }
                            }
                        }
                    }
                    recyclerAdapter.models = getModel<BannerModels>().list
                }
            }
            val linearSnapHelper1 = LinearSnapHelper()
            addType<PlaylistSquareModel>(R.layout.layout_rv) {
                LayoutRvBinding.bind(itemView).recycler.apply {
                    linearSnapHelper1.attachToRecyclerView(this)
                    if (itemDecorationCount == 0)
                        addItemDecoration(HorizontalAlbumItemDecoration())
                    linear(orientation = RecyclerView.HORIZONTAL).setup {
                        addType<PlaylistModel>(R.layout.item_playlist) {
                            val model = getModel<PlaylistModel>()
                            ItemPlaylistBinding.bind(itemView).apply {
                                playlistCover.load(
                                    model.picUrl
                                )
                                playlistTitle.text = model.name
                                itemView.setOnClickListener {
                                    Navigation.findViewNavController(it)
                                        .navigate(
                                            PlaylistFragment(model.id)
                                        )
                                }
                            }
                        }
                    }
                    recyclerAdapter.models = getModel<PlaylistSquareModel>().playlist
                }
            }
            val linearSnapHelper = LinearSnapHelper()
            addType<RecommendPlaylist>(R.layout.layout_rv) {
                LayoutRvBinding.bind(itemView).recycler.apply {
                    linearSnapHelper.attachToRecyclerView(this)
                    if (itemDecorationCount == 0)
                        addItemDecoration(HorizontalAlbumItemDecoration())
                    linear(orientation = RecyclerView.HORIZONTAL).setup {
                        addType<PlaylistModel>(R.layout.item_playlist) {
                            val model = getModel<PlaylistModel>()
                            ItemPlaylistBinding.bind(itemView).apply {
                                playlistTitle.text = model.name
                                playlistCover.load(
                                    model.picUrl
                                )
                                playlistCover.setOnClickListener {
                                    Navigation.findViewNavController(it)
                                        .navigate(
                                            PlaylistFragment(model.id)
                                        )
                                }
                            }
                        }
                    }
                    recyclerAdapter.models = getModel<RecommendPlaylist>().playlist!!
                }
            }
        }
        mBinding.stateLayout.showLoading {
            mEvent.getCookieLiveData().lazy { cookie ->
                viewLifecycleOwner.lifecycle.coroutineScope.launch(Dispatchers.IO) {
                    try {
                        ServiceCreator.create(NeteaseCloudMusicApi::class.java).let {
                            val bannerModel = it.getBannerList().execute()
                            val recommendPlaylist =
                                it.getRecommendPlaylist(cookie).execute()
                            val playlistSquare = it.getPlaylistSquare().execute()
                            val recommendedSong = it.getRecommendedSong(cookie).execute()
                            withContext(Dispatchers.Main) {
                                mBinding.recycler.recyclerAdapter.headers = listOf(
                                    TitleModel(context.getString(R.string.browse)),
                                    bannerModel,
                                    SubTitleModel(context.getString(R.string.recommend_playlist)),
                                    recommendPlaylist,
                                    SubTitleModel(context.getString(R.string.playlist_square)),
                                    playlistSquare,
                                    SubTitleModel(context.getString(R.string.recommend_song))
                                )
                                mBinding.recycler.addModels(recommendedSong.list!!)
                                mBinding.stateLayout.showContentState()
                            }
                        }
                    } catch (e: AnnaException) {
                        mBinding.stateLayout.showErrorState()
                    }
                }
            }
        }
        mBinding.stateLayout.showError {
            mBinding.recycler.recyclerAdapter.clearModels()
        }
        mBinding.stateLayout.showLoadingState()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }
}