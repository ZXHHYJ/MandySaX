package studio.mandysa.music.ui.view;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * 如果这段代码能够正常工作，那么请记住作者是Simon。
 * 如果不能正常工作，那我也不知道是谁写的。
 */
public class BlurTextView extends TextView {
    public BlurTextView(Context context) {
        this(context, null);
    }

    public BlurTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BlurTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setBlurRadius(float radius) {
        if (radius == 0f) {
            getPaint().setMaskFilter(null);
            return;
        }
        BlurMaskFilter blurMaskFilter = new BlurMaskFilter(radius, BlurMaskFilter.Blur.NORMAL);
        getPaint().setMaskFilter(blurMaskFilter);
    }
}
