package studio.mandysa.music.ui.activity.main.helper

import android.view.View
import mandysax.fragment.Fragment
import mandysax.fragment.activityViewModels
import mandysax.navigation.fragment.NavHostFragment
import studio.mandysa.music.ui.event.NavigationViewModel

object NavFragmentHelper {
    fun create(fragment: Fragment): NavHostFragment {
        val navHostFragment = BaseNavHostFragment()
        navHostFragment.navController.navigate(fragment)
        return navHostFragment
    }

    class BaseNavHostFragment : NavHostFragment() {
        private val mViewModel: NavigationViewModel by activityViewModels()
        override fun onViewCreated(view: View) {
            super.onViewCreated(view)
            mViewModel.fragmentLiveData.observe(
                viewLifecycleOwner
            ) { p1: Fragment? -> if (p1 != null) navController.navigate(p1) }
        }

    }
}