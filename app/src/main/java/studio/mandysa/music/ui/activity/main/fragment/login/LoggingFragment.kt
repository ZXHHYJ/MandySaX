package studio.mandysa.music.ui.activity.main.fragment.login

import android.view.View
import mandysax.fragment.activityViewModels
import studio.mandysa.music.logic.ktx.toast
import studio.mandysa.music.ui.activity.main.fragment.all.loading.LoadingDialogFragment
import studio.mandysa.music.ui.event.EventViewModel

class LoggingFragment(private val mobilePhone: String, private val password: String) :
    LoadingDialogFragment() {
    private val mEvent by activityViewModels<EventViewModel>()
    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        mEvent.login(mobilePhone, password).lazy {
            when (it) {
                null -> {}
                else -> {
                    requireActivity().toast(it.msg)
                }
            }
            dismiss()
        }
    }
}