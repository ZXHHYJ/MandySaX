package studio.mandysa.music.ui.activity.main.fragment.all.album

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mandysax.fragment.Fragment
import mandysax.fragment.viewModels
import studio.mandysa.jiuwo.utils.*
import studio.mandysa.music.R
import studio.mandysa.music.databinding.ItemAlbumHeaderBinding
import studio.mandysa.music.databinding.ItemSongBinding
import studio.mandysa.music.databinding.LayoutStateRvBinding
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.model.AlbumContentModel
import studio.mandysa.music.logic.model.MusicModel
import studio.mandysa.music.service.playmanager.PlayManager
import studio.mandysa.music.service.playmanager.ktx.allArtist
import studio.mandysa.music.ui.activity.main.fragment.all.songmenu.SongMenuFragment

class AlbumFragment(private val mAlbumId: String) : Fragment() {

    private val mViewModel by viewModels<AlbumViewModel>()

    private val mBinding by viewBinding<LayoutStateRvBinding>()

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        mBinding.apply {
            recycler.linear().setup {
                addType<AlbumContentModel>(R.layout.item_album_header) {
                    val model = getModel<AlbumContentModel>()
                    ItemAlbumHeaderBinding.bind(itemView).apply {
                        albumCover.load(model.picUrl)
                        albumTitle.text = model.name
                        albumArtistName.text = model.artistName
                        albumDescription.text = model.description
                    }
                }
                addType<MusicModel>(R.layout.item_song) {
                    val model = getModel<MusicModel>()
                    ItemSongBinding.bind(itemView).apply {
                        songIndex.text = (modelPosition + 1).toString()
                        songName.text = model.title
                        songSingerName.text = model.artist.allArtist()
                        itemView.setOnClickListener {
                            PlayManager.loadPlaylist(
                                models,
                                modelPosition
                            )
                        }
                        more.setOnClickListener {
                            SongMenuFragment(model).show(childFragmentManager)
                        }
                    }
                }
            }
            stateLayout.showLoading {
                mViewModel.init(mAlbumId).observe(viewLifecycleOwner) {
                    when (it) {
                        is AlbumViewModel.Status.Header -> {
                            mBinding.recycler.addHeader(it.value)
                            mBinding.stateLayout.showContentState()
                        }
                        is AlbumViewModel.Status.Next -> {
                            mBinding.recycler.addModels(it.value)
                        }
                        is AlbumViewModel.Status.Error -> {
                            mBinding.recycler.recyclerAdapter.clearModels()
                            mBinding.stateLayout.showErrorState()
                        }
                    }
                }
            }
            stateLayout.showError {
                mBinding.recycler.recyclerAdapter.clearModels()
            }
            stateLayout.showLoadingState()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }
}