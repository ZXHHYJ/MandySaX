package studio.mandysa.music.ui.activity.main.fragment.start

import android.annotation.SuppressLint
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.drake.spannable.addSpan
import com.drake.spannable.setSpan
import mandysax.fragment.Fragment
import mandysax.fragment.activityViewModels
import studio.mandysa.music.R
import studio.mandysa.music.databinding.FragmentStartBinding
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.ui.activity.main.fragment.login.LoginFragment
import studio.mandysa.music.ui.event.EventViewModel

class StartFragment : Fragment() {

    private val mBinding by viewBinding<FragmentStartBinding>()

    private val mEvent by activityViewModels<EventViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return mBinding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        ViewCompat.setOnApplyWindowInsetsListener(root) { _, insets ->
            val navigationBarSize =
                insets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
            view.setPadding(
                0,
                0,
                0,
                navigationBarSize
            )
            insets
        }
        mEvent.getCookieLiveData().observe(viewLifecycleOwner) {
            requireActivity().onBackPressed()
        }
        mBinding.slogan.text =
            context.getString(R.string.login_to).setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.translucent_white
                    )
                )
            ).addSpan(context.getString(R.string.app_name))
        mBinding.btnLogin.setOnClickListener {
            LoginFragment().show(childFragmentManager)
        }
        mBinding.btnExit.setOnClickListener {
            requireActivity().finish()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        //返回时还没有登录就退出app
        if (mEvent.isNotLogin()) {
            requireActivity().finish()
        }
    }
}