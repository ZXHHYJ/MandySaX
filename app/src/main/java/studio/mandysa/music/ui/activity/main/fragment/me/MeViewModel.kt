package studio.mandysa.music.ui.activity.main.fragment.me

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mandysax.anna2.exception.AnnaException
import mandysax.lifecycle.ViewModel
import mandysax.lifecycle.livedata.LiveData
import mandysax.lifecycle.livedata.MutableLiveData
import mandysax.lifecycle.viewModelScope
import studio.mandysa.music.R
import studio.mandysa.music.logic.model.NeteaseCloudMusicApi
import studio.mandysa.music.logic.model.design.SubTitleModel
import studio.mandysa.music.logic.model.design.TitleModel
import studio.mandysa.music.logic.network.ServiceCreator

class MeViewModel : ViewModel() {
    sealed class Status {
        data class Next(val value: List<Any>) : Status()
        data class Error(val e: AnnaException) : Status()
    }

    private val mStatusLiveData = MutableLiveData<Status>()

    fun bind(context: Context, cookie: String): LiveData<Status> {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                ServiceCreator.create(NeteaseCloudMusicApi::class.java).let {
                    val userInfo = it.getUserInfo(
                        cookie,
                        System.currentTimeMillis()
                    ).execute()
                    val userPlaylist = it.getUserPlaylist(userInfo.userId).execute()
                    mStatusLiveData.postValue(
                        Status.Next(
                            listOf(
                                TitleModel(
                                    context.getString(R.string.me)
                                ),
                                userInfo,
                                SubTitleModel(
                                    context.resources.getString(R.string.me_playlist)
                                ),
                                userPlaylist,
                                SubTitleModel(
                                    "播放历史"
                                )
                            )
                        )
                    )
                }
            } catch (e: AnnaException) {
                mStatusLiveData.postValue(Status.Error(e))
            }
        }
        return mStatusLiveData
    }
}